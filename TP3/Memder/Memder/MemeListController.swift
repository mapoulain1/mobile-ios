//
//  MemeListController.swift
//  Memder
//
//  Created by Maxime POULAIN on 07/02/2023.
//

import UIKit

class MemeListController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    var memes : Memes?
    var service : ApiService?
    var refreshControl: UIRefreshControl?
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var picker: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        service = ApiService()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.dataSource = self
        tableView.delegate = self
        refreshControl = UIRefreshControl()
        refreshControl!.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        tableView.addSubview(refreshControl!)
        
        
        
        picker.dataSource = self
        picker.delegate = self
        
        self.reload(self)
    }
    
    @IBAction func reload(_ sender: Any) {
        self.spinner.startAnimating()
        self.spinner.hidesWhenStopped = true
        
        self.memes = Memes()
        self.tableView.reloadData()
        
        
        service?.getMemes {(result) in
            switch result{
            case .success(let memes):
                self.memes = memes
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                    self?.spinner.isHidden = true
                    self?.spinner.stopAnimating()
                    self?.refreshControl?.endRefreshing()
                    self?.tableView.reloadData()

                }

            case .failure(let error):
                print(error)
            }
        
        }
  
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.memes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = self.memes?.memes[indexPath.row].title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let memeController = MemeController()
        memeController.meme = memes?.memes[indexPath.row]
        
        self.navigationController?.pushViewController(memeController, animated: true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return service?.subreddit.count ?? 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return service?.subreddit[row] ?? ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        service?.selectedSubreddit = service?.subreddit[row] ?? "rance"
        reload(self)    
    }
    
    @objc func pullToRefresh(_ sender: Any ){
        reload(self)
    }
    
}
