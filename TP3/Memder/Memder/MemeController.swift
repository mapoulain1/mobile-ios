//
//  MemeController.swift
//  Memder
//
//  Created by Maxime POULAIN on 07/02/2023.
//

import UIKit

class MemeController: UIViewController {
    
    var meme: Meme?

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        name.text = meme?.title

        if(meme?.url != nil){
            image.load(url: URL(string: meme?.url ?? "")!)
        }
        
        
    }

    
}

extension UIImageView {
    func load(url: URL){
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url){
                if let image = UIImage(data: data){
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
